package com.video.core;

import static org.junit.Assert.*;

import org.junit.Test;

public class AuditionReportTest {


	@Test
	public void testGenerateReport() {
		AuditionReport auditionReport = new AuditionReport();
		String expected = "tap - 772 - dancer"+"\n"+"tap - 771 - dancer"+"\n"+"324 - performer"+"\n"+"325 - performer"+"\n"+"326 - performer"+"\n"+"I sing in the key of - G - 1191"+"\n"+"I sing in the key of - G - at the volume 5 - 1192"+"\n"; 
	    String actual =auditionReport.generateReport();
	    assertEquals(expected, actual);
	}

}
