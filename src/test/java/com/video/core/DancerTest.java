package com.video.core;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DancerTest {

	private Dancer dancerObject;

	@Before
	public void doSetUp() {
		// int uniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
		dancerObject = new Dancer(772, "dancer", "tap");
	}

	@Test
	public void testDancerPerformance() {
		String actual = dancerObject.perform();
		String expected = "tap - 772 - dancer";
		assertEquals(expected, actual);

	}

}
