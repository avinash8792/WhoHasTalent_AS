package com.video.core;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PerformerTest {

	private Performer performerObject;

	@Before
	public void doSetUp() {
		performerObject = new Performer(324, "performer");
	}

	@Test
	public void testPerformerPerformance() {
		String actual = performerObject.perform();
		String expected = "324 - performer";
		assertEquals(expected, actual);
	}

}
