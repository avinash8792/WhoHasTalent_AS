package com.video.core;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VocalistTest {

	private Vocalist vocalistObject;
	private Random random;

	/*
	 * @Before public void doSetUp() { random = new Random(); }
	 */

	@Test
	public void testVocalistPerformance() {
		vocalistObject = new Vocalist(1191, "vocalist", "G");
		String actual = vocalistObject.perform();
		String expected = "I sing in the key of - G - 1191";
		assertEquals(expected, actual);

	}

	@Test
	public void testVocalistPerformance_WithVolume() {
		// random = new Random();
		// int volume = random.nextInt(11 - 1) + 1;
		int volume = 5;
		vocalistObject = new Vocalist(1191, "vocalist", "G", volume);
		String actual = vocalistObject.perform(volume);
		String expected = "I sing in the key of - G - at the volume 5 - 1191";
		assertEquals(expected, actual);

	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testVolumeMaxBoundary() {
		int volume = 11;
		vocalistObject = new Vocalist(1191, "vocalist", "G", volume);
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Volume should be between 1 and 10.");
		vocalistObject.perform(volume);
	}

	@Rule
	public ExpectedException thrown2 = ExpectedException.none();

	@Test
	public void testVolumeMinBoundary() {
		int volume = 0;
		vocalistObject = new Vocalist(1191, "vocalist", "G", volume);
		thrown2.expect(IllegalArgumentException.class);
		thrown2.expectMessage("Volume should be between 1 and 10.");
		vocalistObject.perform(volume);
	}

}
