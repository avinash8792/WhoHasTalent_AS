package com.video.core;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class Performance_ParameterizedTest {

	private Audition input;
	private String expected;

	public Performance_ParameterizedTest(Audition input, String expected) {
		this.input = input;
		this.expected = expected;
	}

	@Parameterized.Parameters
	public static Collection performances() {
		return Arrays.asList(new Object[][] { { new Dancer(772, "dancer", "tap"), "tap - 772 - dancer" },
				{ new Dancer(771, "dancer", "tap"), "tap - 771 - dancer" },
				{ new Performer(324, "performer"), "324 - performer" },
				{ new Performer(325, "performer"), "325 - performer" },
				{ new Performer(326, "performer"), "326 - performer" },
				{ new Vocalist(1191, "vocalist", "G"), "I sing in the key of - G - 1191" },
				{ new Vocalist(1192, "vocalist", "G", 5), "I sing in the key of - G - at the volume 5 - 1192" } });
	}

	@Test
	public void test() {
		String actual = "";
		if (input.performer_type.equals("vocalist")) {
			Vocalist vocalist = (Vocalist) input;
			if (vocalist.volume != 0) {
				actual = vocalist.perform(vocalist.volume);
			} else {
				actual = vocalist.perform();
			}
		} else {
			actual = input.perform();
		}

		assertEquals(expected, actual);
	}

}
