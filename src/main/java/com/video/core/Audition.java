package com.video.core;

public abstract class Audition {

	final int union_id;
	String performer_type;

	// Audition Constructor overloaded
	public Audition(int union_id, String performer_type) {
		this.union_id = union_id;
		this.performer_type = performer_type;
	}

	public abstract String perform();

}
