package com.video.core;

import java.util.Arrays;
import java.util.List;

public class AuditionReport {

	public String generateReport() {
		
		
		StringBuffer result  = new StringBuffer();
		List<Audition> performersList = Arrays.asList(
				   new Dancer(772, "dancer", "tap"),
				   new Dancer(771, "dancer", "tap"), 
				   new Performer(324, "performer"),
				   new Performer(325, "performer"),
				   new Performer(326, "performer"),
				   new Vocalist(1191, "vocalist", "G"),
				   new Vocalist(1192, "vocalist", "G", 5)
				 );
		
		for(Audition audition : performersList) {
			if(audition.performer_type.equals("vocalist")) {
				Vocalist vocalist = (Vocalist)audition;
				if(vocalist.volume!=0) {
					 result.append(vocalist.perform(vocalist.volume));
					 result.append("\n");
				}
				else {
					 result.append(vocalist.perform());
					 result.append("\n");
				}
			}
			else {
				result.append(audition.perform());
				result.append("\n");
			}
		}
		return result.toString();
		
	}
}
