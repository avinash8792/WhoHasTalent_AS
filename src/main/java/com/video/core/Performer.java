package com.video.core;

public class Performer extends Audition {

	// Performer Constructor overloaded
	public Performer(int union_id, String performer_type) {
		super(union_id, performer_type);

	}

	@Override
	public String perform() {

		return union_id + " - " + performer_type;
	}

}
