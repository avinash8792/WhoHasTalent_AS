package com.video.core;

public class Vocalist extends Audition {

	String key;
	int volume;

	// Vocalist Constructor overloaded
	public Vocalist(int union_id, String performer_type, String key) {
		super(union_id, performer_type);
		this.key = key;

	}

	// Vocalist Constructor overloaded
	public Vocalist(int union_id, String performer_type, String key, int volume) {
		super(union_id, performer_type);
		this.key = key;
		this.volume = volume;
	}

	@Override
	public String perform() {

		return "I sing in the key of - " + key + " - " + union_id;
	}

	// Overloaded Method
	public String perform(int volume) {
		if (volume < 1 || volume > 10) {
			throw new IllegalArgumentException("Volume should be between 1 and 10.");
		} else {

			return "I sing in the key of - " + key + " - " + "at the volume " + volume + " - " + union_id;
		}
	}

}
