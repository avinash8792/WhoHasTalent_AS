package com.video.core;

public class Dancer extends Audition {

	String style;

	// Dancer Constructor overloaded
	public Dancer(int union_id, String performer_type, String style) {
		super(union_id, performer_type);
		this.style = style;
	}

	@Override
	public String perform() {

		String output = style + " - " + union_id + " - " + performer_type;
		return output;

	}

}
